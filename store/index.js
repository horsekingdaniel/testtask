export const state = () => ({
  option: 0
})

export const mutations = {
  changeOption(state, option) {
    state.option = option;
  }
}

export const getters = {
  getOption: (state) => {
      return state.option;
  },
}